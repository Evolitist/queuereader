package com.evolitist.queuereader.utils;

import android.graphics.Typeface;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class FB2Parser
{
	private List<FormattedString> content;
	private NodeList nodes = null;
	private File fb2File;
	private int fontSize;

	private FB2Parser(){}

	public static FB2Parser newInstance(File file)
	{
		FB2Parser instance = new FB2Parser();
		instance.fb2File = file;
		return instance;
	}

	public FB2Parser parse()
	{
		try
		{
			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(fb2File);
			nodes = new NodeList(
					new NodeList(doc.getChildNodes()).byName("FictionBook").getChildNodes());
		}
		catch(SAXException | ParserConfigurationException | IOException e)
		{
			e.printStackTrace();
		}
		return this;
	}

	public String getFB2Path()
	{
		return fb2File.getAbsolutePath();
	}

	private List<FormattedString> compact(List<FormattedString> l)
	{
		List<FormattedString> list = new ArrayList<>();
		int j = 0;
		list.add(l.get(j));
		for(int i = 1; i < l.size(); i++)
		{
			if(l.get(i).formatEquals(list.get(j))) list.get(j).appendParagraph(l.get(i).content);
			else
			{
				list.add(l.get(i));
				j++;
			}
		}
		return list;
	}

	public String getBookTitle()
	{
		if(nodes == null)
		{ throw new IllegalStateException("Attempt to use an uninitialized FB2Parser"); }
		String[] names = new String[]{"description", "title-info", "book-title"};
		Node tempNode;
		NodeList tempList = nodes;
		for(String name : names)
		{
			tempNode = tempList.byName(name);
			if(tempNode == null) throw new MalformedFB2Exception();
			tempList = new NodeList(tempNode.getChildNodes());
		}
		Node bookTitleText = tempList.byName("#text");
		if(bookTitleText == null) throw new MalformedFB2Exception("Empty \"book-title\" tag");
		return bookTitleText.getNodeValue();
	}

	public String getBookAuthor()
	{
		if(nodes == null)
		{ throw new IllegalStateException("Attempt to use an uninitialized FB2Parser"); }
		String[] names = new String[]{"description", "title-info", "author"};
		Node tempNode;
		NodeList tempList = nodes;
		for(String name : names)
		{
			tempNode = tempList.byName(name);
			if(tempNode == null) throw new MalformedFB2Exception();
			tempList = new NodeList(tempNode.getChildNodes());
		}
		Node fnNode = tempList.byName("first-name");
		Node lnNode = tempList.byName("last-name");
		Node nnNode = tempList.byName("nickname");
		if(fnNode != null && lnNode != null)
		{
			Node fnText = new NodeList(fnNode.getChildNodes()).byName("#text");
			Node lnText = new NodeList(lnNode.getChildNodes()).byName("#text");
			if((fnText.getNodeValue().isEmpty() || lnText.getNodeValue().isEmpty()) &&
					nnNode != null)
			{
				return new NodeList(nnNode.getChildNodes()).byName("#text").getNodeValue();
			}
			else
			{
				Node mnNode = tempList.byName("middle-name");
				Node mnText = null;
				if(mnNode != null) mnText = new NodeList(mnNode.getChildNodes()).byName("#text");
				String mid = " " + (mnText != null && !mnText.getNodeValue().isEmpty() ?
						mnText.getNodeValue() + " " : "");
				return fnText.getNodeValue() + mid + lnText.getNodeValue();
			}
		}
		else
		{
			Node nnText = new NodeList(nnNode.getChildNodes()).byName("#text");
			return nnText.getNodeValue();
		}
	}

	public String getBookGenres()
	{
		if(nodes == null)
		{ throw new IllegalStateException("Attempt to use an uninitialized FB2Parser"); }
		String[] names = new String[]{"description", "title-info"};
		Node tempNode;
		NodeList tempList = nodes;
		for(String name : names)
		{
			tempNode = tempList.byName(name);
			if(tempNode == null) throw new MalformedFB2Exception();
			tempList = new NodeList(tempNode.getChildNodes());
		}
		Node[] genres = tempList.listByName("genre");
		String[] genresText = new String[genres.length];
		for(int i = 0; i < genres.length; i++)
		{
			genresText[i] = new NodeList(genres[i].getChildNodes()).byName("#text").getNodeValue();
		}
		return TextUtils.join(", ", genresText);
	}

	public List<FormattedString> getContent(boolean compact, int fs)
	{
		fontSize = fs;
		NodeList bodyList = new NodeList(nodes.byName("body").getChildNodes());
		content = new ArrayList<>();
		content.add(new FB2Parser.FormattedString(" ", fontSize * 2, 0, 0));
		if(bodyList.hasName("title")) parseTitle(bodyList.byName("title"));
		for(Node n : bodyList.listByName("section"))
		{
			parseSection(n);
		}
		content.add(new FB2Parser.FormattedString(" ", fontSize * 2, 0, 0));
		return compact ? compact(content) : content;
	}

	private void parseSection(Node sectionNode)
	{
		for(Node n : new NodeList(sectionNode.getChildNodes()))
		{
			switch(n.getNodeName())
			{
				case "title":
					parseTitle(n);
					break;
				case "epigraph":
					parseEpigraph(n, 0);
					break;
				case "p":
					parseParagraph(n, 1, 0, 0);
					break;
				case "empty-line":
					content.add(new FormattedString(" ", fontSize, 0, 0));
					break;
				case "subtitle":
					parseParagraph(n, 1, 1, 0);
					break;
				case "cite":
					parseCite(n, 0);
					break;
				case "poem":
					parsePoem(n, 0, 0);
					break;
				case "section":
					parseSection(n);
					break;
			}
		}
	}

	private void parseTitle(Node titleNode)
	{
		for(Node n : new NodeList(titleNode.getChildNodes()))
		{
			switch(n.getNodeName())
			{
				case "p":
					parseParagraph(n, fontSize * 2, 0, 1, 0, Gravity.CENTER_HORIZONTAL);
					break;
				case "empty-line":
					content.add(new FormattedString(" ", fontSize * 2, 1, 0));
					break;
			}
		}
		content.add(new FormattedString(" ", fontSize, 0, 0));
	}

	private void parseParagraph(Node pNode, int margin, int style, int depth)
	{
		parseParagraph(pNode, fontSize, margin, style, depth, 0);
	}

	private void parseParagraph(Node pNode, int fontSize, int margin, int style, int depth, int gravity)
	{
		NodeList pText = new NodeList(pNode.getChildNodes());
		if(pText.size() > 0)
		{
			StringBuilder builder = new StringBuilder();
			for(int i = 0; i < depth*5 + margin; i++) builder.append("   ");
			for(Node t : pText)
			{
				if(t.getNodeName().equals("#text")) builder.append(t.getNodeValue());
				else if(t.getNodeName().equals("style"))
					builder.append(t.getFirstChild().getNodeValue());
			}
			content.add(new FormattedString(builder.toString(), fontSize - depth*2, style, gravity));
		}
	}

	private void parseCite(Node citeNode, int depth)
	{
		for(Node n : new NodeList(citeNode.getChildNodes()))
		{
			switch(n.getNodeName())
			{
				case "p":
					parseParagraph(n, 1, 2, depth);
					break;
				case "empty-line":
					content.add(new FormattedString(" ", fontSize - depth*2, 2, 0));
					break;
				case "subtitle":
					parseParagraph(n, 1, 3, depth);
					break;
				case "poem":
					parsePoem(n, 2, depth);
					break;
				case "text-author":
					parseParagraph(n, 2, 2, depth);
					break;
			}
		}
	}

	private void parsePoem(Node poemNode, int style, int depth)
	{
		content.add(new FormattedString(" ", fontSize - depth*2, style, 0));
		for(Node n : new NodeList(poemNode.getChildNodes()))
		{
			switch(n.getNodeName())
			{
				case "title":
					parseParagraph(n, 3, style, depth);
					break;
				case "epigraph":
					parseEpigraph(n, 1);
					break;
				case "subtitle":
					parseParagraph(n, 1, style | 1, depth);
					break;
				case "stanza":
					for(Node nn : new NodeList(n.getChildNodes()))
					{
						switch(nn.getNodeName())
						{
							case "subtitle":
								parseParagraph(nn, 1, style | 1, depth);
								break;
							case "v":
								parseParagraph(nn, 2, style, depth);
								break;
						}
					}
					break;
				case "text-author":
					parseParagraph(n, 2, style, depth);
					break;
				case "date":
					parseParagraph(n, 3, style, depth);
					break;
			}
			if(!n.getNodeName().equals("#text")) content.add(new FormattedString(" ", fontSize -
					depth*2, style, 0));
		}
	}

	private void parseEpigraph(Node epigraphNode, int d)
	{
		int depth = d + 1;
		for(Node n : new NodeList(epigraphNode.getChildNodes()))
		{
			switch(n.getNodeName())
			{
				case "p":
					parseParagraph(n, 0, 2, depth);
					break;
				case "poem":
					parsePoem(n, 2, depth);
					break;
				case "cite":
					parseCite(n, depth);
					break;
				case "empty-line":
					content.add(new FormattedString(" ", fontSize - depth*2, 2, 0));
					break;
				case "text-author":
					parseParagraph(n, 1, 2, depth);
					break;
			}
		}
	}

	private class MalformedFB2Exception extends IllegalStateException
	{
		public MalformedFB2Exception()
		{
			super("Invalid FB2 content");
		}

		public MalformedFB2Exception(String desc)
		{
			super("Invalid FB2 content: " + desc);
		}
	}

	public static class FormattedString implements Parcelable
	{
		public final int style, gravity;
		public final float size;
		private String content;

		public FormattedString(String s, float tSize, int tStyle, int tGravity)
		{
			content = s;
			size = tSize;
			style = tStyle;
			gravity = tGravity;
		}

		protected FormattedString(Parcel in)
		{
			int[] info = new int[2];
			in.readIntArray(info);
			style = info[0];
			gravity = info[1];
			size = in.readFloat();
			content = in.readString();
		}

		public static final Creator<FormattedString> CREATOR = new Creator<FormattedString>()
		{
			@Override
			public FormattedString createFromParcel(Parcel in)
			{
				return new FormattedString(in);
			}

			@Override
			public FormattedString[] newArray(int size)
			{
				return new FormattedString[size];
			}
		};

		public void appendParagraph(String s)
		{
			content += "\n" + s;
		}

		public String getContent()
		{
			return content;
		}

		public void formatTextView(TextView textView)
		{
			textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
			textView.setTypeface(Typeface.DEFAULT, style);
			textView.setGravity(gravity);
			textView.setText(content);
		}

		public boolean formatEquals(FormattedString fs)
		{
			return size == fs.size && style == fs.style && gravity == fs.gravity;
		}

		@Override
		public int describeContents()
		{
			return 0;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags)
		{
			dest.writeString(content);
			dest.writeFloat(size);
			dest.writeIntArray(new int[]{style, gravity});
		}
	}
}
