package com.evolitist.queuereader.utils;

import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by evolitist on 5/2/16.
 */
public class NodeList implements Iterable<Node>
{
	private final org.w3c.dom.NodeList source;

	public NodeList(org.w3c.dom.NodeList src)
	{
		source = src;
	}

	public Node item(int i)
	{
		return source.item(i);
	}

	public int size()
	{
		return source.getLength();
	}

	public boolean hasName(String s)
	{
		for(Node n : this)
		{
			if(n.getNodeName().equalsIgnoreCase(s)) return true;
		}
		return false;
	}

	public Node byName(String s)
	{
		for(Node n : this)
		{
			if(n.getNodeName().equalsIgnoreCase(s)) return n;
		}
		return null;
	}

	public Node[] listByName(String s)
	{
		List<Node> nl = new ArrayList<>();
		for(Node n : this)
		{
			if(n.getNodeName().equalsIgnoreCase(s)) nl.add(n);
		}
		Node[] na = new Node[nl.size()];
		return nl.toArray(na);
	}

	@Override
	public Iterator<Node> iterator()
	{
		return new Iterator<Node>()
		{
			private int cursor = 0;

			@Override
			public boolean hasNext()
			{
				return cursor < source.getLength();
			}

			@Override
			public Node next()
			{
				if(hasNext())
				{
					return source.item(cursor++);
				}
				throw new NoSuchElementException();
			}

			@Override
			public void remove()
			{
				throw new UnsupportedOperationException();
			}
		};
	}
}
