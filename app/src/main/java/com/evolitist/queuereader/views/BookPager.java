package com.evolitist.queuereader.views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evolitist.queuereader.R;
import com.evolitist.queuereader.activities.PagedReaderActivity;

import java.util.ArrayList;
import java.util.List;

import static com.evolitist.queuereader.utils.FB2Parser.FormattedString;

public class BookPager extends ViewPager
{
	public final AsyncTask<Integer, FormattedString[], Void> pageTask =
			new AsyncTask<Integer, FormattedString[], Void>()
	{
		private DisplayMetrics dm;
		private PagerAdapter adapter;

		@Override
		protected void onPreExecute()
		{
			dm = getResources().getDisplayMetrics();
			adapter = getAdapter();
		}

		@Override
		protected Void doInBackground(Integer... params)
		{
			final float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, dm),
					wf = params[0] - padding * 2, hf = params[1] - padding * 2;
			List<FormattedString> fsList = new ArrayList<>();
			FormattedString[] fsArray;
			float hc = 0;
			Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
			paint.setStyle(Paint.Style.STROKE);
			for(int i = 0, contentSize = stringList.size(); i < contentSize && !isCancelled(); i++)
			{
				FormattedString fs = stringList.get(i);
				paint.setTextSize(TypedValue.applyDimension(
						TypedValue.COMPLEX_UNIT_SP, fs.size, dm));
				paint.setTypeface(Typeface.create(Typeface.DEFAULT, fs.style));
				float lineHeight = decreasedSpacing ? paint.getFontMetrics().descent -
						paint.getFontMetrics().ascent : (paint.getFontMetrics().bottom -
						paint.getFontMetrics().top);
				String s = fs.getContent();
				while(!s.isEmpty() && !isCancelled())
				{
					if(hc + lineHeight < hf)
					{
						int lineLength = paint.breakText(s, true, wf, null);
						int lastSpace = Math.max(s.substring(0, lineLength).lastIndexOf(' '),
								s.substring(0, lineLength).lastIndexOf('-'));
						lastSpace = lastSpace == -1 ? lineLength - 1 : lastSpace;
						fsList.add(new FormattedString(lineLength == s.length() ? s :
								s.substring(0, lastSpace + 1), fs.size, fs.style, fs.gravity));
						s = lineLength == s.length() ? "" : s.substring(lastSpace + 1);
						hc += lineHeight;
					}
					else
					{
						fsArray = fsList.toArray(new FormattedString[fsList.size()]);
						publishProgress(fsArray);
						hc = 0;
						fsList = new ArrayList<>();
					}
				}
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(FormattedString[]... values)
		{
			contents.add(values[0]);
			adapter.notifyDataSetChanged();
		}

		@Override
		protected void onCancelled()
		{
			((Activity)getContext()).finish();
		}
	};
	private final List<FormattedString> stringList = new ArrayList<>();
	private List<FormattedString[]> contents = new ArrayList<>();
	private boolean decreasedSpacing;

	public BookPager(Context context)
	{
		super(context);
		decreasedSpacing = PreferenceManager.getDefaultSharedPreferences(context)
				.getBoolean("decreased_spacing", false);
	}

	public BookPager(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		decreasedSpacing = PreferenceManager.getDefaultSharedPreferences(context)
				.getBoolean("decreased_spacing", false);
	}

	public void setContent(List<FormattedString> content)
	{
		stringList.clear();
		stringList.addAll(content);
	}

	@Override
	protected void onSizeChanged(final int w, final int h, int oldw, int oldh)
	{
		pageTask.execute(w, h);
		super.onSizeChanged(w, h, oldw, oldh);
	}

	public PageAdapter newAdapter(FragmentManager fm)
	{
		return new PageAdapter(fm);
	}

	public class PageAdapter extends FragmentStatePagerAdapter
	{
		public PageAdapter(FragmentManager fm)
		{
			super(fm);
		}

		@Override
		public Fragment getItem(int position)
		{
			return PageFragment.newInstance(contents.get(position));
		}

		@Override
		public int getCount()
		{
			return contents.size();
		}
	}

	public static class PageFragment extends Fragment implements View.OnClickListener
	{
		private FormattedString[] fsList;

		public PageFragment(){}

		public static PageFragment newInstance(FormattedString[] fsl)
		{
			PageFragment f = new PageFragment();
			Bundle args = new Bundle();
			args.putParcelableArray("content", fsl);
			f.setArguments(args);
			return f;
		}

		@Override
		public void onCreate(Bundle savedInstanceState)
		{
			super.onCreate(savedInstanceState);
			fsList = (FormattedString[])getArguments().getParcelableArray("content");
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			View rootView = inflater.inflate(R.layout.fragment_page, container, false);
			rootView.setOnClickListener(this);
			PageView pageView = (PageView)rootView.findViewById(R.id.page_view);
			pageView.updateWithContent(fsList);
			return rootView;
		}

		@Override
		public void onClick(View v)
		{
			((PagedReaderActivity)getActivity()).toggle();
		}
	}
}
