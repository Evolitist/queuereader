package com.evolitist.queuereader.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import com.evolitist.queuereader.utils.FB2Parser;

public class PageView extends View
{
	private final Paint textPaint;
	private FB2Parser.FormattedString[] content = new FB2Parser.FormattedString[0];
	private boolean decreasedSpacing;

	public PageView(Context context)
	{
		super(context);
		decreasedSpacing = PreferenceManager.getDefaultSharedPreferences(context)
				.getBoolean("decreased_spacing", false);
	}

	public PageView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		decreasedSpacing = PreferenceManager.getDefaultSharedPreferences(context)
				.getBoolean("decreased_spacing", false);
	}

	public PageView(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		decreasedSpacing = PreferenceManager.getDefaultSharedPreferences(context)
				.getBoolean("decreased_spacing", false);
	}

	{
		textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		textPaint.setStyle(Paint.Style.STROKE);
	}

	public void updateWithContent(FB2Parser.FormattedString[] l)
	{
		content = l;
		invalidate();
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		float lineHeight;
		for(FB2Parser.FormattedString aContent : content)
		{
			textPaint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
					aContent.size, getResources().getDisplayMetrics()));
			textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, aContent.style));
			lineHeight = decreasedSpacing ? textPaint.getFontMetrics().descent -
					textPaint.getFontMetrics().ascent : (textPaint.getFontMetrics().bottom -
					textPaint.getFontMetrics().top);
			canvas.translate(0, lineHeight);
			canvas.drawText(aContent.getContent(), 0, 0, textPaint);
		}
	}
}