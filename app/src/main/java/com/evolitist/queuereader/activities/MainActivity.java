package com.evolitist.queuereader.activities;

import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.evolitist.queuereader.R;
import com.evolitist.queuereader.fragments.QueueFragment;

public class MainActivity extends AppCompatActivity
		implements NavigationView.OnNavigationItemSelectedListener
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getFragmentManager().beginTransaction().replace(R.id.fragment_main, new QueueFragment())
				.commit();
		Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		FloatingActionButton fab;
		if((fab = (FloatingActionButton)findViewById(R.id.fab)) != null)
		{
			fab.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setType("message/rfc822");
					i.putExtra(Intent.EXTRA_EMAIL, new String[]{"evolitist@gmail.com"});
					i.putExtra(Intent.EXTRA_SUBJECT, "QueueReader bug report");
					try
					{
						startActivity(Intent.createChooser(i, "Send mail..."));
					}
					catch(ActivityNotFoundException ex)
					{
						Toast.makeText(MainActivity.this, "There are no email clients installed.",
								Toast.LENGTH_SHORT).show();
					}
				}
			});
		}
		DrawerLayout drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
				this, drawer, toolbar, R.string.navigation_drawer_open,
				R.string.navigation_drawer_close);
		if(drawer != null)
		{
			drawer.addDrawerListener(toggle);
		}
		toggle.syncState();
		NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
		if(navigationView != null)
		{
			navigationView.setNavigationItemSelectedListener(this);
		}
	}

	@Override
	protected void onNewIntent(Intent intent)
	{
		if(Intent.ACTION_SEARCH.equals(intent.getAction()))
		{
			String query = intent.getStringExtra(SearchManager.QUERY);
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
	{
		switch(requestCode)
		{
			case 113:
			{
				if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
				{
					QueueFragment queueFragment =
							(QueueFragment)getFragmentManager()
									.findFragmentById(R.id.fragment_main);
					queueFragment.initQueue();
				}
			}
		}
	}

	@Override
	public void onBackPressed()
	{
		DrawerLayout drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
		if(drawer != null && drawer.isDrawerOpen(GravityCompat.START))
		{
			drawer.closeDrawer(GravityCompat.START);
		}
		else
		{
			super.onBackPressed();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		int id = item.getItemId();
		//noinspection SimplifiableIfStatement
		if(id == R.id.action_settings)
		{
			startActivity(new Intent(this, SettingsActivity.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onNavigationItemSelected(MenuItem item)
	{
		int id = item.getItemId();
		if(id == R.id.nav_queue)
		{
			getFragmentManager().beginTransaction().replace(R.id.fragment_main, new QueueFragment())
					.commit();
		}
		else if(id == R.id.nav_settings)
		{
			startActivity(new Intent(this, SettingsActivity.class));
		}
		DrawerLayout drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
		if(drawer != null)
		{
			drawer.closeDrawer(GravityCompat.START);
		}
		return true;
	}
}
