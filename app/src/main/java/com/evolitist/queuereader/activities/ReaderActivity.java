package com.evolitist.queuereader.activities;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.evolitist.queuereader.R;
import com.evolitist.queuereader.utils.FB2Parser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ReaderActivity extends AppCompatActivity
{
	private String fb2Path;
	private AppBarLayout mAppbar;
	private RecyclerView mContentView;
	private ProgressBar mProgressView;
	private int mDecorSize;
	private boolean mUpdOnScroll, isShowing = true;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
		{
			getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
		}
		mUpdOnScroll = PreferenceManager.getDefaultSharedPreferences(this)
				.getBoolean("ui_change_on_scroll", true);
		setContentView(R.layout.activity_reader);
		Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		ActionBar ab = getSupportActionBar();
		if(ab != null)
		{
			ab.show();
			ab.setDisplayHomeAsUpEnabled(true);
		}
		mAppbar = (AppBarLayout)findViewById(R.id.appbar);
		mProgressView = (ProgressBar)findViewById(R.id.progress_loading);
		mContentView = (RecyclerView)findViewById(R.id.fullscreen_content);
		if(mUpdOnScroll)
		{
			initScrollHandlers();
		}
		fb2Path = String.valueOf(getIntent().getCharSequenceExtra("fb2"));
		mContentView.setLayoutManager(new LinearLayoutManager(this));
		mContentView.setAdapter(new BookTextAdapter());
	}

	private void initScrollHandlers()
	{
		View abContainer = findViewById(R.id.appbar_container);
		assert abContainer != null;
		AppBarLayout.LayoutParams abParams =
				(AppBarLayout.LayoutParams)abContainer.getLayoutParams();
		abParams.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
				| AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP
				| AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
		//CoordinatorLayout.LayoutParams clParams =
		//		(CoordinatorLayout.LayoutParams)mContentView.getLayoutParams();
		//clParams.setBehavior(new AppBarLayout.ScrollingViewBehavior());
		TypedValue tv = new TypedValue();
		if(getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
		{
			mDecorSize = TypedValue.complexToDimensionPixelSize(tv.data,
					getResources().getDisplayMetrics()) + (int)TypedValue.applyDimension(
					TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics());
		}
		mAppbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener()
		{
			@Override
			public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset)
			{
				if(Math.abs(verticalOffset) == mDecorSize)
				{
					hideSystemUI();
				}
				else
				{
					showSystemUI();
				}
			}
		});
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		if(!isShowing) hideSystemUI();
		else showSystemUI();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		int id = item.getItemId();
		if(id == android.R.id.home)
		{
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


	private void toggle()
	{
		if(getSupportActionBar() != null && getSupportActionBar().isShowing())
		{
			hide();
		}
		else
		{
			show();
		}
	}

	private void hide()
	{
		mAppbar.animate().translationY(-mAppbar.getBottom())
				.setInterpolator(new AccelerateInterpolator())
				.setListener(new Animator.AnimatorListener()
				{
					@Override
					public void onAnimationStart(Animator animation)
					{
						isShowing = false;
						hideSystemUI();
					}

					@Override
					public void onAnimationEnd(Animator animation)
					{
						ActionBar ab = getSupportActionBar();
						if(ab != null)
						{
							ab.hide();
						}
						View padding = findViewById(R.id.padding_view);
						if(padding != null)
						{
							padding.setVisibility(View.GONE);
						}
					}

					@Override
					public void onAnimationCancel(Animator animation){}

					@Override
					public void onAnimationRepeat(Animator animation){}
				}).start();
	}

	@SuppressLint("InlinedApi")
	private void hideSystemUI()
	{
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
		{
			getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN
					| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
					| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
					| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
		}
		else
		{
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}
	}

	private void show()
	{
		mAppbar.animate().translationY(0)
				.setInterpolator(new DecelerateInterpolator())
				.setListener(new Animator.AnimatorListener()
				{
					@Override
					public void onAnimationStart(Animator animation)
					{
						isShowing = true;
						showSystemUI();
						View padding = findViewById(R.id.padding_view);
						if(padding != null)
						{
							padding.setVisibility(View.VISIBLE);
						}
						ActionBar ab = getSupportActionBar();
						if(ab != null)
						{
							ab.show();
						}
					}

					@Override
					public void onAnimationEnd(Animator animation){}

					@Override
					public void onAnimationCancel(Animator animation){}

					@Override
					public void onAnimationRepeat(Animator animation){}
				}).start();
	}

	private void showSystemUI()
	{
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
		{
			getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
					| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
		}
		else
		{
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}
	}

	private class BookTextAdapter extends RecyclerView.Adapter<BookTextAdapter.BookTextItemHolder>
	{
		private final List<FB2Parser.FormattedString> mContent = new ArrayList<>();

		private BookTextAdapter()
		{
			new AsyncTask<String, Void, List<FB2Parser.FormattedString>>()
			{
				@Override
				protected List<FB2Parser.FormattedString> doInBackground(String... params)
				{
					String fb2Path = params[0];
					int fontSize = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(
							ReaderActivity.this).getString("font_size", "16"));
					return FB2Parser.newInstance(new File(fb2Path)).parse().getContent(true,
							fontSize);
				}

				@Override
				protected void onPostExecute(List<FB2Parser.FormattedString> layout)
				{
					mContent.addAll(layout);
					notifyDataSetChanged();
					mProgressView.setVisibility(View.INVISIBLE);
					mContentView.setVisibility(View.VISIBLE);
				}
			}.execute(fb2Path);
		}

		@Override
		public BookTextItemHolder onCreateViewHolder(ViewGroup parent, int viewType)
		{
			View v = getLayoutInflater().inflate(R.layout.item_book_text, parent, false);
			return new BookTextItemHolder(v);
		}

		@Override
		public void onBindViewHolder(BookTextItemHolder holder, int position)
		{
			mContent.get(position).formatTextView(holder.contentView);
		}

		@Override
		public int getItemCount()
		{
			return mContent.size();
		}

		public class BookTextItemHolder extends RecyclerView.ViewHolder implements
				View.OnClickListener
		{
			private final TextView contentView;

			public BookTextItemHolder(View itemView)
			{
				super(itemView);
				if(!mUpdOnScroll) itemView.setOnClickListener(this);
				contentView = (TextView)itemView.findViewById(R.id.book_text_view);
			}

			@Override
			public void onClick(View v)
			{
				toggle();
			}
		}
	}
}
