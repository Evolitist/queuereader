package com.evolitist.queuereader.activities;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.evolitist.queuereader.R;
import com.evolitist.queuereader.utils.FB2Parser;
import com.evolitist.queuereader.views.BookPager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PagedReaderActivity extends AppCompatActivity
{
	private AppBarLayout mAppbar;
	private BookPager mContentView;
	private boolean isShowing = true;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
		{
			getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
		}
		setContentView(R.layout.activity_reader_paged);
		Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		ActionBar ab = getSupportActionBar();
		if(ab != null)
		{
			ab.show();
			ab.setDisplayHomeAsUpEnabled(true);
		}
		String fb2Path = String.valueOf(getIntent().getCharSequenceExtra("fb2"));
		mAppbar = (AppBarLayout)findViewById(R.id.appbar);
		mContentView = (BookPager)findViewById(R.id.fullscreen_content);
		int fontSize = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this)
				.getString("font_size", "16"));
		mContentView.setContent(FB2Parser.newInstance(new File(fb2Path)).parse().getContent(false,
				fontSize));
		mContentView.setAdapter(mContentView.newAdapter(getSupportFragmentManager()));
		hide();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		if(!isShowing) hideSystemUI();
		else showSystemUI();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		int id = item.getItemId();
		if(id == android.R.id.home)
		{
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed()
	{
		mContentView.pageTask.cancel(true);
		super.onBackPressed();
	}

	public void toggle()
	{
		if(getSupportActionBar() != null && getSupportActionBar().isShowing())
		{
			hide();
		}
		else
		{
			show();
		}
	}

	private void hide()
	{
		mAppbar.animate().translationY(-mAppbar.getBottom())
				.setInterpolator(new AccelerateInterpolator())
				.setListener(new Animator.AnimatorListener()
				{
					@Override
					public void onAnimationStart(Animator animation)
					{
						isShowing = false;
						hideSystemUI();
					}

					@Override
					public void onAnimationEnd(Animator animation)
					{
						ActionBar ab = getSupportActionBar();
						if(ab != null)
						{
							ab.hide();
						}
						View padding = findViewById(R.id.padding_view);
						if(padding != null)
						{
							padding.setVisibility(View.GONE);
						}
					}

					@Override
					public void onAnimationCancel(Animator animation){}

					@Override
					public void onAnimationRepeat(Animator animation){}
				}).start();
	}

	@SuppressLint("InlinedApi")
	private void hideSystemUI()
	{
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
		{
			getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN
					| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
					| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
					| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
		}
		else
		{
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}
	}

	private void show()
	{
		mAppbar.animate().translationY(0)
				.setInterpolator(new DecelerateInterpolator())
				.setListener(new Animator.AnimatorListener()
				{
					@Override
					public void onAnimationStart(Animator animation)
					{
						isShowing = true;
						showSystemUI();
						View padding = findViewById(R.id.padding_view);
						if(padding != null)
						{
							padding.setVisibility(View.VISIBLE);
						}
						ActionBar ab = getSupportActionBar();
						if(ab != null)
						{
							ab.show();
						}
					}

					@Override
					public void onAnimationEnd(Animator animation){}

					@Override
					public void onAnimationCancel(Animator animation){}

					@Override
					public void onAnimationRepeat(Animator animation){}
				}).start();
	}

	private void showSystemUI()
	{
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
		{
			getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
					| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
		}
		else
		{
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}
	}
}
