package com.evolitist.queuereader.fragments;

import android.Manifest;
import android.app.Fragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.evolitist.queuereader.R;
import com.evolitist.queuereader.activities.PagedReaderActivity;
import com.evolitist.queuereader.activities.ReaderActivity;
import com.evolitist.queuereader.utils.FB2Parser;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

public class QueueFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener
{
	public QueueAdapter adapter;
	private SwipeRefreshLayout refresh;
	private RecyclerView queue;
	private ProgressBar progress;
	private boolean hasExternalStorageAccess;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_queue, container, false);
		refresh = (SwipeRefreshLayout)rootView.findViewById(R.id.queue_refresh);
		refresh.setColorSchemeResources(R.color.colorAccent, R.color.colorPrimaryDark);
		refresh.setOnRefreshListener(this);
		queue = (RecyclerView)rootView.findViewById(R.id.queue);
		progress = (ProgressBar)rootView.findViewById(R.id.progress_loading);
		hasExternalStorageAccess = ContextCompat.checkSelfPermission(getActivity(),
				Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
		if(!hasExternalStorageAccess)
		{
			ActivityCompat.requestPermissions(getActivity(),
					new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 113);
		}
		else
		{
			initQueue();
		}
		return rootView;
	}

	public void initQueue()
	{
		queue.setLayoutManager(new LinearLayoutManager(getActivity()));
		adapter = new QueueAdapter();
		queue.setAdapter(adapter);
		progress.setVisibility(View.INVISIBLE);
		refresh.setVisibility(View.VISIBLE);
	}

	@Override
	public void onRefresh()
	{
		new Handler().postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				refresh.setRefreshing(false);
			}
		}, 4000);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		inflater.inflate(R.menu.queue, menu);
		SearchManager searchManager =
				(SearchManager)getActivity().getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView)menu.findItem(R.id.action_search).getActionView();
		searchView.setSearchableInfo(
				searchManager.getSearchableInfo(getActivity().getComponentName()));
	}

	private class QueueItem
	{
		public final Drawable cover;
		public final String title, author, genres, fb2Path;

		public QueueItem(FB2Parser p)
		{
			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
			{
				cover = getResources().getDrawable(R.drawable.book_cover_dummy, null);
			}
			else
			{
				cover = getResources().getDrawable(R.drawable.book_cover_dummy);
			}
			title = p.getBookTitle();
			author = p.getBookAuthor();
			genres = p.getBookGenres();
			fb2Path = p.getFB2Path();
		}
	}

	private class QueueAdapter extends RecyclerView.Adapter<QueueAdapter.QueueItemHolder>
	{
		private final List<QueueItem> mDataset;

		private QueueAdapter()
		{
			this.mDataset = new ArrayList<>();
			if(hasExternalStorageAccess)
			{
				File booksFolder = new File(Environment.getExternalStorageDirectory()
						.getAbsolutePath() + "/Books");
				if(!booksFolder.exists()) booksFolder.mkdir();
				for(File book : booksFolder.listFiles(new FilenameFilter()
				{
					@Override
					public boolean accept(File dir, String filename)
					{
						return filename.endsWith(".fb2");
					}
				}))
				{
					FB2Parser bookParser = FB2Parser.newInstance(book).parse();
					mDataset.add(new QueueItem(bookParser));
				}
			}
		}

		@Override
		public QueueItemHolder onCreateViewHolder(ViewGroup parent, int viewType)
		{
			View v = LayoutInflater.from(parent.getContext())
					.inflate(R.layout.item_queue, parent, false);
			return new QueueItemHolder(v);
		}

		@Override
		public void onBindViewHolder(QueueItemHolder holder, int position)
		{
			holder.cover.setImageDrawable(mDataset.get(position).cover);
			holder.title.setText(mDataset.get(position).title);
			holder.author.setText(mDataset.get(position).author);
			holder.genre.setText(mDataset.get(position).genres);
		}

		@Override
		public int getItemCount()
		{
			return mDataset.size();
		}

		public void removeItem(int id)
		{
			mDataset.remove(id);
			notifyItemRemoved(id);
		}

		public class QueueItemHolder extends RecyclerView.ViewHolder
				implements View.OnClickListener, View.OnLongClickListener
		{
			public ImageView cover;
			public TextView title, author, genre;

			public QueueItemHolder(View itemView)
			{
				super(itemView);
				itemView.setOnClickListener(this);
				itemView.setOnLongClickListener(this);
				cover = (ImageView)itemView.findViewById(R.id.book_cover);
				title = (TextView)itemView.findViewById(R.id.book_title);
				author = (TextView)itemView.findViewById(R.id.book_author);
				genre = (TextView)itemView.findViewById(R.id.book_genre);
			}

			@Override
			public void onClick(View v)
			{
				//Snackbar.make(v, "Selected \"" + title.getText() + "\"", Snackbar.LENGTH_LONG)
				//		.setAction("Action", null).show();
				boolean paged = PreferenceManager.getDefaultSharedPreferences(getActivity())
						.getBoolean("reader_paged", true);
				Intent intent = new Intent(getActivity(), paged ? PagedReaderActivity.class :
						ReaderActivity.class);
				intent.putExtra("fb2", mDataset.get(getAdapterPosition()).fb2Path);
				startActivity(intent);
			}

			@Override
			public boolean onLongClick(View v)
			{
				v.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
				removeItem(getLayoutPosition());
				return true;
			}
		}
	}
}
