package com.evolitist.queuereader.fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.evolitist.queuereader.R;

/**
 * Created by evolitist on 4/29/16.
 */
public class SettingsFragment extends PreferenceFragment
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
	}
}
